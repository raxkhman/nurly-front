import { useEffect, useState } from "react";
import { axiosInstance } from "../../common/config/axiosInstance";



export const TransactionsPage = () => {
    const [categoryName, setCategoryName] = useState('');

    useEffect(()=> {
        (async () => {
            const response = await axiosInstance.get("/Category/List");
            if (response.status === 200) {
            }
        })()
    }, [])

    const handleCategoryCreation = async () => {
        const response = await axiosInstance.get("/Category/Create?categoryName="+categoryName);
        if (response.status === 200) {
            (async () => {
                const response = await axiosInstance.get("/Category/List");
                if (response.status === 200) {
                }
            })()
        }
    } 

    return (
        <>
            <h1>Categories</h1>
           
            <h1>Add</h1>
            <input value={categoryName} onChange={e => setCategoryName(e.target.value)} /> <br />
            <button onClick={handleCategoryCreation}>Create category</button>
        </>
    );
};
